<?php
/**
 * Unit Test bootstrap.
 *
 * @package     Grofftech\MetaBoxGenerator\Tests\Unit
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\MetaBoxGenerator\Tests\Unit;

define( 'META_BOX_GENERATOR_TESTS_DIRECTORY', __DIR__ );
define(
    'META_BOX_GENERATOR_PLUGIN_DIRECTORY',
    dirname( dirname( dirname( __DIR__ ) ) )
);

$composer_autoload = META_BOX_GENERATOR_PLUGIN_DIRECTORY . '/vendor/autoload.php';

if ( file_exists( $composer_autoload ) ) {
    require_once $composer_autoload;
}