<?php
/**
 * Test Configuration
 *
 * @package     Grofftech\PhoenixTimber\Tests\Unit\Config
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\PhoenixTimber\Tests\Unit\Config;

return array(
    'character.c3po' => array(
        'details' => array(
            'name' => 'C3PO',
            'occupation' => 'droid',
            'catch_phrase' => '',
        ),
        'hero_status' => false,
    ),
);
