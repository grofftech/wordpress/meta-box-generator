# Meta Box Generator

## Requirements

- Composer
- Git
- GNU Make
- Node / NPM

## Setup

- Clone this repository to your WordPress `wp-content/plugins` folder
- From a command prompt navigate to the root plugin folder
- Run `composer install`

## How To Use

### Metabox Configuration

Rename `lib/MetaBox/Config/Sample.php` based on the meta box that you are configuring (e.g. Article.php). Update the `metabox.sample` key in the array to the name of the meta box you are configuring (e.g. metabox.article).

For additional meta box configurations, copy and rename `lib/MetaBox/Config/Defaults.php` based on the meta box you are configuring. Replace the `metabox.unique_key` with your own unique_key, making sure to keep the `metabox.` part.

Configure the metabox options and add as many custom fields as needed. See [https://developer.wordpress.org/plugins/metadata/custom-meta-boxes/](https://developer.wordpress.org/plugins/metadata/custom-meta-boxes/) for more information about custom meta boxes.

### Views

Rename `lib/MetaBox/Views/Sample.php` based on the meta box that you are configuring (e.g Article.php). Update the view file markup to suit your needs.

For additional meta box configurations, create a new view file and update the associated meta box configurations.

### Loading Configuration(s)

In the run method of `lib\MetaBox\MetaBox.php` add the applicable meta box configuration files to the `$configuration_files` array (rename and use sample as a guide).

## Assets

From a command prompt, navigate to the plugin root directory and run `npm install`. This will install dependencies required for building the front-end assets (CSS and JavaScript).

### CSS

CSS is using SASS. Update the sample module in the `assets/src/sass/modules` and use it as your guide for adding additional CSS.

### JavaScript

Each JavaScript file in `assets/src/js` is complied individually as both a non-minified and minified file. Provide each path of the JavaScript file in the `JS_FILES` variable in the `Makefile`. The `js-files` task in the `Makefile` uses the `scripts/compile-js-files.sh` bash script.

### Build

To run the front-end build process, open a command prompt and from the plugin root directory run `make`
