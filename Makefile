# Executables
SASS=node_modules/.bin/node-sass
POST_CSS=node_modules/.bin/postcss
STYLELINT=node_modules/.bin/stylelint
WATCH=node_modules/.bin/watch
TERSER=node_modules/.bin/terser
ESLINT=node_modules/.bin/eslint

# Configuration
CONFIG_DIR=config
STYLELINT_CONFIG=$(CONFIG_DIR)/stylelintrc.json
ESLINT_CONFIG=$(CONFIG_DIR)/.eslintrc.json
MAKE_OPTIONS=--no-print-directory

# Sources
SOURCE_DIR=assets/src

CSS_SOURCE_DIR=$(SOURCE_DIR)/sass

SASS_FILE=$(SOURCE_DIR)/sass/style.scss
SASS_FILES=$(SOURCE_DIR)/sass/modules/*

JS_SOURCE_DIR=$(SOURCE_DIR)/js

# A list of Javascript files so we can enqueue individually
JS_FILES=""

LANGUAGE_FILES=""

# Distribution
DIST_DIR=assets/dist

# CSS directory
BUILD_CSS_DIR=$(DIST_DIR)/css

# Style files
BUILD_CSS_FILE=$(BUILD_CSS_DIR)/style.css
BUILD_CSS_FILE_MIN=$(BUILD_CSS_DIR)/style.min.css

# JavaScript directory
BUILD_JS_DIR=$(DIST_DIR)/js

# Images directory
BUILD_IMAGES_DIR=$(DIST_DIR)/images

# Languages directory
BUILD_LANGUAGES_DIR=$(DIST_DIR)/languages

all: clean $(BUILD_CSS_FILE) js-files

$(BUILD_CSS_FILE): $(BUILD_CSS_DIR) sass-compile cssnano

$(BUILD_CSS_DIR):
	@ echo "Creating directory '$(BUILD_CSS_DIR)'..."
	@ mkdir -p $@

sass-compile: stylelint
	@ echo "Compiling Sass..."
	@ $(SASS) $(SASS_FILE) \
		--output $(BUILD_CSS_DIR) \
		--output-style expanded \
		--source-map true \
		--quiet

stylelint:
	@ echo "Linting Sass..."
	@ $(STYLELINT) $(SASS_FILES) \
		--config $(STYLELINT_CONFIG)

cssnano: autoprefixer
	@ echo "Minifying CSS..."
	@ $(POST_CSS) \
		-u $@ \
		-o $(BUILD_CSS_FILE_MIN) \
		$(BUILD_CSS_FILE)

autoprefixer:
	@ echo "Autoprefixing CSS..."
	@ $(POST_CSS) \
		-u $@ \
		-r $(BUILD_CSS_FILE)

js-files: lint-js $(BUILD_JS_DIR)
	@echo "Minifying JS Files..."
	@./scripts/compile-js-files.sh $(JS_FILES) $(BUILD_JS_DIR)

$(BUILD_JS_DIR):
	@ echo "Creating directory '$(BUILD_JS_DIR)'..."
	@ mkdir -p $@

lint-js:
	@ echo "Linting JS..."
	@ $(ESLINT) \
		--config $(ESLINT_CONFIG) \
		$(JS_SOURCE_DIR)

clean:
	@ echo "Removing directory '$(DIST_DIR)'..."
	@ rm -rf $(DIST_DIR)

.PHONY: all clean sass-compile stylelint autoprefixer cssnano lint-js js-files
