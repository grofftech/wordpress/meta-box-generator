<?php
/**
 * Meta Box.
 *
 * @since       1.0.0
 * @author      Brett Groff
 * @see        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\MetaBoxGenerator\MetaBox;

use Grofftech\MetaBoxGenerator\Config\ConfigService;
use Grofftech\MetaBoxGenerator\Service\Service;

class MetaBox extends Service
{
    /**
     * Configuration Service class.
     */
    private $config_service;

    public function __construct( ConfigService $config_service )
    {
        $this->config_service = $config_service;
    }

    /**
    * Runs methods required for meta boxes.
    * Overrides the abstract service class run method.
    *
    * @since 1.0.0
    *
    * @return void
    */
    public function run() {
        $this->register_hooks();

        $configuration_files = array(
            __DIR__ . '/Config/Sample.php'
        );

        $this->load_configurations( $configuration_files );
    }

    /**
     * Registers hooks required for meta boxes.
     *
     * @since 1.0.0
     */
    public function register_hooks()
    {
        \add_action( 'admin_menu', array( $this, 'register_meta_boxes' ) );
        \add_action( 'save_post', array ( $this, 'save_meta_boxes' ) );
    }

    /**
     * Loads the metabox configurations.
     *
     * @since 1.0.0
     */
    private function load_configurations( $configuration_files )
    {
        $defaults = ( array ) require __DIR__.'/Config/Defaults.php';
        $defaults = current( $defaults );

        foreach ( $configuration_files as $config_file ) {
            $this->config_service->load_configuration(
                $config_file,
                $defaults
            );
        }
    }

    /**
     * Registers the meta boxes with Wordpress.
     *
     * @since 1.0.0
     */
    public function register_meta_boxes()
    {
        foreach ( ( array )$this->get_meta_box_keys() as $key ) {
            $config = $this->config_service->get_configuration_parameters(
                $key,
                'add_meta_box'
            );

            add_meta_box(
                $this->get_meta_box_id( $key ),
                $config['title'],
                array( $this, 'render_meta_box' ), // callback function
                $config['screen'],
                $config['context'],
                $config['priority'],
                $config['callback_args']
            );
        }
    }

    /**
    * Gets the metabox configurations by keys.
    *
    * @since 1.0.0
    *
    * @return void
    */
    private function get_meta_box_keys() {
        return $this->config_service->get_configuration_keys_starting_with( 'metabox.' );
    }

    /**
     * Gets the meta box id, which is the value after the period.
     *
     * @since 1.0.0
     *
     * @param string $key the configuration key
     *
     * @return string The meta box id
     */
    private function get_meta_box_id( $key )
    {
        return str_replace('metabox.', '', $key);
    }

    /**
     * Renders the meta box.
     *
     * @since 1.0.0
     *
     * @param WP_Post $post Instance of the post for this meta box.
     * @param array $meta_box Array of meta box arguments.
     *
     * @return void
     */
    public function render_meta_box( \WP_Post $post, array $meta_box ) {
        $meta_box_id = $meta_box["id"];
        $config = $this->config_service->get_configuration('metabox.' . $meta_box_id);
        $custom_fields = array();

	    // Security with a nonce
    	\wp_nonce_field( $meta_box_id . '_nonce_action', $meta_box_id . '_nonce_name' );

        // Only grab custom field data if we are including custom fields
        if ( $config['add_meta_box']['include_custom_fields'] ) {
            $custom_fields = $this->get_custom_field_values(
                $post->ID,
                $meta_box_id,
                $config
            );
        }

        include $config['view'];
    }

    /**
     * Gets the values of the custom fields from the database.
     * If there is nothing returned from the database, uses the default
     * configuration values.
     *
     * @since 1.0.0
     *
     * @param integer $post_id The post ID.
     * @param string $meta_box_id The metabox ID.
     * @param array $config The configuration.
     *
     * @return void
     */
    private function get_custom_field_values($post_id, $meta_box_id, array $config)
    {
        $custom_fields = array();

        foreach ( $config['custom_fields'] as $meta_key => $custom_field_config ) {
            $custom_fields[$meta_key] = \get_post_meta(
                $post_id,
                $meta_key,
                $custom_field_config['is_single']
            );

            if ( ! $custom_fields[$meta_key] ) {
                $custom_fields[$meta_key] = $custom_field_config['default'];
            }
        }

        /**
         * Filters the custom fields.
         *
         * @since 1.0.0
         *
         * @return array An array of custom field values, the meta box id and post id.
         */
        return ( array )\apply_filters(
            'filter_custom_field_values',
            $custom_fields,
            $meta_box_id,
            $post_id
        );
    }

    /**
     * Save the metaboxes.
     *
     * @since 1.0.0
     *
     * @param integer $post_id Post ID.
     *
     * @return void
     */
    public function save_meta_boxes( $post_id ) {
        foreach( ( array )$this->get_meta_box_keys() as $key ) {
            $meta_box_id = $this->get_meta_box_id( $key );

            $config = $this->config_service->get_configuration( $key );

            if ( ! $this->is_okay_to_save_meta_box( $meta_box_id ) ) {
                continue;
            }

            if ( $config['add_meta_box']['include_custom_fields'] ) {
                $this->save_custom_fields(
                    $config['custom_fields'],
                    $meta_box_id,
                    $post_id
                );
            }
        }
    }

    /**
     * Checks to see if metabox is valid for saving.
     *
     * @since 1.0.0
     *
     * @param string $meta_box_id Meta Box ID.
     *
     * @return bool False if it fails validation checks, otherwise true.
     */
    private function is_okay_to_save_meta_box( $meta_box_id )
    {
        if ( ! array_key_exists( $meta_box_id, $_POST ) ) {
            return false;
        }

        // if autosaving, ajax or future posting bail
        if ( ( defined( 'DOING_AUTOSAVE' ) && 'DOING_AUTOSAVE' )
             || ( defined( 'DOING_AJAX' ) && 'DOING_AJAX' )
             || ( defined( 'DOING_CRON' ) && 'DOING_CRON' ) ) {
            return false;
        }

        // If the nonce doesn't match, returns false.
        return \wp_verify_nonce(
            $_POST[$meta_box_id . '_nonce_name'],
            $meta_box_id . '_nonce_action'
        );
    }

    /**
    * Saves the custom fields.
    *
    * @since 1.0.0
    *
    * @param array $config The configuration options.
    * @param string $meta_box_key The meta box key.
    * @param int $post_id The post ID.
    *
    * @return void
    */
    private function save_custom_fields( $config, $meta_box_key, $post_id ) {
        $config = $this->remap_custom_fields_config( $config );

        // Merge what we get back from POST with default values so that we always have data
        $metadata = wp_parse_args(
            $_POST[$meta_box_key],
            $config['default']
        );

        foreach ( $metadata as $meta_key => $value ) {
            // If value is in delete state, delete custom field metadata from db
            if ( $config['delete_state'][$meta_key] === $value ) {
                \delete_post_meta( $post_id, $meta_key );
                continue;
            }

            $sanitize_function = $config['sanitize'][$meta_key];
            $value = $sanitize_function( $value );

            \update_post_meta( $post_id, $meta_key, $value );
        }
    }

    /**
     * Remaps the custom fields configuration so it can be used for saving.
     *
     * @since 1.0.0
     *
     * @param array $config The custom fields configuration.
     *
     * @return array The custom fields configuration with keys remapped.
     */
    private function remap_custom_fields_config( array $config )
    {
        $remapped_config = array(
            'is_single' => array(),
            'default' => array(),
            'delete_state' => array(),
            'sanitize' => array()
        );

        foreach ( $config as $meta_key => $custom_field_config ) {
            foreach ( $custom_field_config as $parameter => $value ) {
                $remapped_config[$parameter][$meta_key] = $value;
            }
        }

        return $remapped_config;
    }
}
