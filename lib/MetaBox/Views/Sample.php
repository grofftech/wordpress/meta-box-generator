<p>
    <label for="">
        <?php _e( 'Sample Label', 'meta-box-generator' ) ?>
    </label>
    <input
        class="large-text"
        type="text"
        name="<?php echo $meta_box_id; ?>[sample_custom_field]"
        value="<?php esc_attr_e( ! empty( $custom_fields ) ? $custom_fields['sample_custom_field'] : ''); ?>">
    <span class="description">
        <?php _e( 'Sample Help Text', 'meta-box-generator' ) ?>
    </span>
</p>