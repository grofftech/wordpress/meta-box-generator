<?php

/**
 * Meta Box Default Configurations
 *
 * @package     Grofftech\MetaBoxGenerator\MetaBox\Config
 * @since       1.0.0
 * @author      groffTECH
 * @license     GNU-2.0+
 *
 * See https://developer.wordpress.org/plugins/metadata/custom-meta-boxes/ for configuration options
 */

namespace Grofftech\MetaBoxGenerator\MetaBox\Config;

return array(
    'metabox.unique_key' => array(

        'add_meta_box' => array(
            'title' => '',
            'screen' => null,
            'context' => 'advanced',
            'priority' => 'default',
            'callback_args' => null,
            'include_custom_fields' => true
        ),

        'custom_fields' => array(
            'meta_key' => array (
                'is_single' => true,
                'default' => '',
                'delete_state' => '',
                'sanitize' => 'sanitize_text_field'
            ),
        ),

        'view' => ''
    )
);