<?php

/**
 * Meta Box Sample Configuration
 *
 * @package     Grofftech\MetaBoxGenerator\MetaBox\Config
 * @since       1.0.0
 * @author      groffTECH
 * @license     GNU-2.0+
 *
 * See https://developer.wordpress.org/plugins/metadata/custom-meta-boxes/ for configuration options
 */

namespace Grofftech\MetaBoxGenerator\MetaBox\Config;

return array(
    'metabox.sample' => array(

        'add_meta_box' => array(
            'title' => __('Sample Meta Box', 'mb'), // The title of the meta box
            'screen' => 'post', // The post type
            'context' => 'advanced', // Where it should be displayed for the screen
            'priority' => 'default', // default, high, low
            'callback_args' => null, // Additional params for the callback
            'include_custom_fields' => false
        ),

        'custom_fields' => array(
            'sample_custom_field' => array(
                'is_single' => true,
                'default' => '',
                'delete_state' => '',
                'sanitize' => 'sanitize_text_field'
            ),
        ),

        'view' => META_BOX_GENERATOR_DIR . 'lib/MetaBox/Views/Sample.php'
    )
);