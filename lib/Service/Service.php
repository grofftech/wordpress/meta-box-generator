<?php
/**
 * Service
 *
 * @package     Grofftech\MetaBoxGenerator\Service
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\MetaBoxGenerator\Service;

use Grofftech\MetaBoxGenerator\Interfaces\Hookable;
use Grofftech\MetaBoxGenerator\Interfaces\Runnable;

/**
 * Service abstract class.
 */
abstract class Service implements Runnable, Hookable {
    /**
     * Run.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function run() {
        $this->register_hooks();
    }
}