<?php
/**
 * Registers services required for the plugin.
 *
 * @package     Grofftech\MetaBoxGenerator\Service
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\MetaBoxGenerator\Service;

use Grofftech\MetaBoxGenerator\Interfaces\Runnable;

abstract class ServiceRegistrar implements Runnable {
    /**
     * The classes to instantiate.
     *
     * @var array
     */
    protected $classes = [];

    /**
     * The dependency injector.
     *
     * @var Grofftech\MetaBoxGenerator\Dependencies\Auryn
     */
    protected $injector;

    /**
     * Run.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function run() {
        $this->register_services();
    }

    /**
     * Register services.
     *
     * @since 1.0.0
     *
     * @return void
     */
    protected function register_services() {
        $this->classes = $this->init_classes();

        foreach ( $this->classes as $class ) {
            $class->run();
        }
    }

    /**
     * Initialize classes.
     *
     * @since 1.0.0
     *
     * @return bool|false|int|string
     */
    protected function init_classes() {
        $objects = array_map( function( $class ) {
            return array(
                'namespace' => $class,
                'object' => $this->injector->make( $class )
            );
        }, $this->classes );

        return array_column( $objects, 'object', 'namespace');
    }
}