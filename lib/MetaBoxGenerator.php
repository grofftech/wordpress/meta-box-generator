<?php
/**
 * Meta Box Generator
 *
 * @package     Grofftech\MetaBoxGenerator
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\MetaBoxGenerator;

use Grofftech\MetaBoxGenerator\Service\ServiceRegistrar;
use Grofftech\MetaBoxGenerator\Config\ConfigService;
use Grofftech\MetaBoxGenerator\MetaBox\MetaBox;
use Grofftech\MetaBoxGenerator\Asset\AssetVersioning;

class MetaBoxGenerator extends ServiceRegistrar {

    /**
     * The directory path of the plugin.
     *
     * @var string
     */
    private $plugin_path;

    /**
     * The url of the plugin.
     *
     * @var string
     */
    private $plugin_url;

    /**
     * The Auryn dependency injector.
     *
     * @var Grofftech\MetaBoxGenerator\Dependencies\Auryn
     */
    protected $injector;

    /**
     * Constructor
     *
     * @param string $plugin_path The directory path of the plugin.
     * @param string $plugin_url  The plugin url.
     * @param Grofftech\MetaBoxGenerator\Dependencies\Auryn $injector The dependency injector.
     */
    public function __construct( $plugin_path, $plugin_url, $injector ) {
        $this->plugin_path = $plugin_path;
        $this->plugin_url = $plugin_url;
        $this->injector = $injector;
    }

    /**
     * Classes to be instantiated.
     */
    protected $classes = array(
        ConfigService::class,
        MetaBox::class,
        AssetVersioning::class
    );

    /**
    * Kicks off plugin functionality.
    *
    * @since 1.0.0
    *
    * @return void
    */
    public function run() {
        $this->init_constants();
        $this->register_hooks();
        parent::run();
    }

    /**
    * Initialize plugin constants.
    *
    * @since 1.0.0
    *
    * @return void
    */
    private function init_constants()
    {
        if ( ! defined( 'META_BOX_GENERATOR_DIR' ) ) {
            define( 'META_BOX_GENERATOR_DIR', $this->plugin_path );
        }

        if ( ! defined( 'META_BOX_GENERATOR_URL' ) ) {
            define( 'META_BOX_GENERATOR_URL', $this->plugin_url );
        }

        if ( ! defined( 'META_BOX_GENERATOR_NAME' ) ) {
            define( 'META_BOX_GENERATOR', 'Meta Box Generator' );
        }

        if ( ! defined( 'META_BOX_GENERATOR_VERSION' ) ) {
            define( 'META_BOX_GENERATOR_VERSION', '1.0.0' );
        }

        if ( ! defined( 'META_BOX_GENERATOR_TEXT_DOMAIN' ) ) {
            define( 'META_BOX_GENERATOR_TEXT_DOMAIN', 'meta-box-generator' );
        }
    }

    /**
    * Registers plugin level hooks.
    *
    * @since 1.0.0
    *
    * @return void
    */
    public function register_hooks() {

    }

    /**
     * Runs anything that needs to be done when
     * the plugin is activated.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public static function activate() {

    }

    /**
     * Runs anything that needs to be done when
     * the plugin is deactivated.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public static function deactivate() {

    }

    /**
     * Runs anything that needs to be done
     * when the plugin is uninstalled.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public static function uninstall() {

    }

    /**
     * Gets a specific class instance.
     *
     * @since 1.0.0
     *
     * @param string $classname The namespace with class name.
     *
     * @return object
     */
    public function get_class( string $classname ) {
        return (object) $this->classes[ $classname ];
    }
}
