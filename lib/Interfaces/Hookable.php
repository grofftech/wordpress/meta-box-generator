<?php
/**
 * Hookable Interface
 *
 * @package     Grofftech\MetaBoxGenerator\Interfaces
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\MetaBoxGenerator\Interfaces;

interface Hookable {
    public function register_hooks();
}