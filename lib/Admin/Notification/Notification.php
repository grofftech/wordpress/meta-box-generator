<?php
/**
 * Notification Handler
 *
 * @package     Grofftech\MetaBoxGenerator\Admin
 * @since       1.0.0
 * @author      Brett Groff
 * @link        https://grofftech.net
 * @license     GNU General Public License 2.0+
 */

namespace Grofftech\MetaBoxGenerator\Admin\Notification;

class Notification {

    public function __construct()
    {

    }

    public function show_error_message( $message ) {
        // Add a custom action so we can use the message parameter
        add_action( 'meta-box-generator-error-message', [ $this, 'render_error_message' ], 10, 1 );

        // Do the custom action with the message
		add_action( 'admin_notices', function() use ( $message ) {
            do_action( 'meta-box-generator-error-message', $message );
        } );
    }

    public function render_error_message( $message ) {
        include __DIR__ . '/Views/Error.php';
    }
}