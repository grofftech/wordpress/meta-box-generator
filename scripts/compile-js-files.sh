#!/bin/bash

TERSER=node_modules/.bin/terser
JS_FILES=$1
BUILD_JS_DIR=$2

for f in $JS_FILES
do
    FILENAME_WITH_EXTENSION="${f##*/}"
    FILENAME_WITHOUT_EXTENSION="${FILENAME_WITH_EXTENSION%.*}"

    cp -v $f $BUILD_JS_DIR/$FILENAME_WITH_EXTENSION

    $TERSER $f \
        --compress \
        --mangle \
        --source-map \
        --output $BUILD_JS_DIR/$FILENAME_WITHOUT_EXTENSION.min.js
done